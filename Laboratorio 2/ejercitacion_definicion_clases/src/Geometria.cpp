#include <iostream>

using namespace std;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho) {};

uint Rectangulo::alto() {
    return this -> alto_;
}

uint Rectangulo::ancho() {
    return this -> ancho_;
}

float Rectangulo::area() {
    return this -> alto_ * ancho_;
}

// Ejercicio 2

class Elipse {
public:
    Elipse(uint a, uint b);
    uint r_a();
    uint r_b();
    float area();

private:
    int radio_a_;
    int radio_b_;

};

Elipse::Elipse(uint r_a, uint r_b) : radio_a_(r_a), radio_b_(r_b) {};

uint Elipse:: r_a() {
    return radio_a_;
}

uint Elipse:: r_b() {
    return radio_b_;
}

float Elipse::area() {
    float pi = 3.14;
    return radio_a_ * radio_b_ * pi;
}

// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return r_.alto();
}

float Cuadrado::area() {
        return  r_.area();
}

// Ejercicio 4

class Circulo {
public:
    Circulo(uint radio);
    uint radio();
    float area();

private:
    Elipse circulo_;
};

Circulo::Circulo(uint radio): circulo_(radio, radio) {}

uint Circulo::radio(){
    return circulo_.r_a();
};

float Circulo::area(){
    return circulo_.area();
}


// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

// ostream Elipse

ostream& operator<<(ostream& os, Elipse e) {
    os << "Elipse(" << e.r_a() << ", " << e.r_b() << ")";
    return os;
}

// Ejercicio 6

ostream& operator<<(ostream& os, Cuadrado c) {
    os << "Cuad(" << c.lado() << ")";
    return os;
}

ostream& operator<<(ostream& os, Circulo c) {
    os << "Circ(" << c.radio() << ")";
    return os;
}