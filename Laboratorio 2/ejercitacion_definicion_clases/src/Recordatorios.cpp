#include <iostream>

using namespace std;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    uint mes();
    uint dia();
    bool operator==(Fecha o);
    bool operator<(Fecha o);
    void incrementar_dia();

  private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia) {};

uint Fecha::mes() {
    return mes_;
}

uint Fecha::dia() {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return (igual_dia && igual_mes);
}

bool Fecha::operator<(Fecha o) {
    bool res = false;
    if ((this->mes() < o.mes()) || (this->mes() == o.mes() && this->dia() < o.dia())){
        res = true;
    }
    return (res);
}

void Fecha::incrementar_dia() {
    if (dias_en_mes(this-> mes())== this -> dia()){
        dia_ = 1;
        mes_ = mes_ + 1;
    } else {
        dia_ = dia_ + 1;
    }
}

// Ejercicio 11, 12

// Clase Horario
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);

private:
    int hora_;
    int min_;
};

Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {};

uint Horario:: hora() {
    return hora_;
}

uint Horario:: min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora();
    bool igual_min = this->min() == h.min();
    return (igual_hora && igual_min);
}

bool Horario::operator<(Horario h) {
    bool res = false;
    if ((this->hora() < h.hora()) || (this -> hora() == h.hora() && this->min() < h.min())){
        res = true;
    }
    return (res);
}

// Ejercicio 13

// Clase Recordatorio
class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario horario, string recordatorio);
    Fecha fecha();
    Horario horario();
    string recordatorio();
    bool operator<(Recordatorio r);

private:
    Fecha fecha_;
    Horario horario_;
    string recordatorio_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string recordatorio): fecha_(fecha), horario_(horario), recordatorio_(recordatorio) {};

Fecha Recordatorio:: fecha() {
    return fecha_;
}

Horario Recordatorio:: horario() {
    return horario_;
}

string Recordatorio:: recordatorio() {
    return recordatorio_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.recordatorio() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

bool Recordatorio::operator<(Recordatorio r) {
    bool res = false;
    if ((fecha() < r.fecha()) || (this->fecha() == r.fecha() && this->horario() < r.horario())) {
        res = true;
    }
    return (res);
}

// Ejercicio 14

#include <list>

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    list<Recordatorio> agenda_;
    Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial): hoy_(fecha_inicial), agenda_() {};

void Agenda::agregar_recordatorio(Recordatorio rec) {
    agenda_.push_back(rec);
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> recordatorios;
    for (Recordatorio rec: agenda_){ // Me quedo solo con los recordatorios del dia
        if (rec.fecha() == hoy_){
            recordatorios.push_back(rec);
        }
    }
    recordatorios.sort();
    return recordatorios;
}

Fecha Agenda:: hoy() {
    return hoy_ ;
}

ostream& operator<<(ostream& os, Agenda agenda) {
    os << agenda.hoy() << endl;
    os<< "=====" << endl;
    for (Recordatorio recordatorio : agenda.recordatorios_de_hoy()){
        os << recordatorio << endl;
    }
    return os;
}