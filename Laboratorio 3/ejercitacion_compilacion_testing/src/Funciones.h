#ifndef SOLUCION_FUNCIONES_H
#define SOLUCION_FUNCIONES_H
#include "Meses.h"

bool esBisiesto(Anio);
int diasEnMes(int, int);

#endif //SOLUCION_FUNCIONES_H
