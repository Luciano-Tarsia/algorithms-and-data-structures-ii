#include <vector>
#include "algobot.h"

using namespace std;

// Ejercicio 1
vector<int> quitar_repetidos(vector<int> s) {
    set<int> set_res;
    for (int i = 0; i < s.size(); i++) {
        if (set_res.count(s[i]) == 0) {
            set_res.insert(s[i]);
        }
    }
    vector<int> res;
    for (int n : set_res) {
        res.push_back(n);
    }
    return res;
}

// Ejercicio 2
vector<int> quitar_repetidos_v2(vector<int> s) {
    set<int> set_res;
    for (int i = 0; i < s.size(); i++) {
        if (set_res.count(s[i]) == 0) {
            set_res.insert(s[i]);
        }
    }
    vector<int> res;
    for (int n : set_res) {
        res.push_back(n);
    }
    return res;
}

// Ejercicio 3
bool mismos_elementos(vector<int> a, vector<int> b) {
    bool res1 = true;
    bool res2 = true;
    set<int> a_aux;
    set<int> b_aux;
    for (int n : a){
        a_aux.insert(n);
    }
    for (int m : b){
        b_aux.insert(m);
        if (a_aux.count(m) == 0){
            res2 = false;
        }
    }
    for (int n : a){
        if (b_aux.count(n) == 0){
            res1 = false;
        }
    }
    return (res1 && res2);
}

// Ejercicio 4
bool mismos_elementos_v2(vector<int> a, vector<int> b) {
    bool res1 = true;
    bool res2 = true;
    set<int> a_aux;
    set<int> b_aux;
    for (int n : a){
        a_aux.insert(n);
    }
    for (int m : b){
        b_aux.insert(m);
        if (a_aux.count(m) == 0){
            res2 = false;
        }
    }
    for (int n : a){
        if (b_aux.count(n) == 0){
            res1 = false;
        }
    }
    return (res1 && res2);
}

// Ejercicio 5
map<int, int> contar_apariciones(vector<int> s) {
    map<int , int> res;
    set<int> aux;
    for (int i = 0; i < s.size() ; ++i) {
        int count = 0;
        if (aux.count(s[i]) == 0) {
            aux.insert(s[i]);
            for (int j = 0; j < s.size(); ++j) {
                if (s[i]==s[j]){
                    count++;
                }
            }
            res[s[i]]=count;
        }
    }
    return res;
}

// Ejercicio 6
vector<int> filtrar_repetidos(vector<int> s) {
    map<int,int> aux = contar_apariciones(s);
    vector<int> res;
    for (int n : s) {
        if (aux[n] == 1){
            res.push_back(n);
        }
    }
    return res;
}

// Ejercicio 7
set<int> interseccion(set<int> a, set<int> b) {
    set<int> interseccion;
    for(int n: a){
        if (b.count(n) >= 1){
            interseccion.insert(n);
        }
    }
    for(int m: b){
        if (a.count(m) >= 1){
            interseccion.insert(m);
        }
    }
    return interseccion;
}

// Ejercicio 8
map<int, set<int>> agrupar_por_unidades(vector<int> s) {
    map<int, set<int>> res;
    for (int n : s) {
        res[n % 10].insert(n);
    }
    return res;
}

// Ejercicio 9
vector<char> traducir(vector<pair<char, char>> tr, vector<char> str) {
    vector<char> res = str;
    map<char, char> m;
    for (int i = 0; i < tr.size(); ++i) {
        m[tr[i].first] = (tr[i].second);
    }
    for (int j = 0; j < str.size() ; ++j) {
        if (m.count(str[j]) > 0) {
            res[j] = m[str[j]];
        }
    }
    return res;
}

// Ejercicio 10
bool integrantes_repetidos(vector<Mail> s) {
    set<set<LU>> conjuntos;
    bool res = false;
    for(Mail M : s){
        conjuntos.insert(M.libretas());
    }
    for (set<LU> libretas1 : conjuntos) {
        for (LU libreta : libretas1) {
            for (set<LU> libretas2 : conjuntos){
                if (libretas1 != libretas2 && libretas2.count(libreta) > 0){
                    res = true;
                }
            }
        }
    }
    return res;
}

// Ejercicio 11
map<set<LU>, Mail> entregas_finales(vector<Mail> s) {
    map<set<LU>, Mail> dic;
    for (Mail mail : s){
        if (dic.count(mail.libretas()) == 0 && mail.adjunto()){
            dic[mail.libretas()] = mail;
        }
        if (dic.count(mail.libretas()) == 1 && dic[mail.libretas()].fecha() < mail.fecha() && mail.adjunto()){
            dic[mail.libretas()] = mail;
        }
    }
  return dic;
}
