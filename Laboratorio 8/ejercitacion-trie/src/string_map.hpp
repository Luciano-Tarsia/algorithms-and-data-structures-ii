
#include "string_map.h"

template<typename T>
string_map<T>::string_map(): raiz(nullptr), _size(0) {}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    destruir(raiz);
    _size = d._size;
    raiz = nullptr;
    if (d.raiz != nullptr) {
        raiz = new Nodo;
        construir(raiz, d.raiz);
    }
    return *this;
}

template<typename T>
string_map<T>::~string_map() {
    destruir(raiz);
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &par) {
    string Clave = par.first;
    T *Significado = new T;
    *Significado = par.second;
    if (count(Clave) == 0) {
        _size++;
    }
    if (raiz == nullptr) {
        raiz = new Nodo;
    }
    Nodo *nodoAux = raiz;
    int i = 0;
    for (char c : Clave) {
        int valor = int(c);
        if (i == Clave.size() - 1) {
            if (nodoAux->siguientes[valor] == nullptr) {
                nodoAux->siguientes[valor] = new Nodo(Significado);
            } else {
                nodoAux = nodoAux->siguientes[valor];
                delete nodoAux->definicion;
                nodoAux->definicion = Significado;
            }
        } else {
            if (nodoAux->siguientes[valor] == nullptr) {
                nodoAux->siguientes[valor] = new Nodo;
                nodoAux = nodoAux->siguientes[valor];
            } else {
                nodoAux = nodoAux->siguientes[valor];
            }
        }
        i++;
    }
}

template<typename T>
int string_map<T>::count(const string &clave) const {
    int res = 0;
    Nodo *nodoAux = raiz;
    int i = 0;
    while (i < clave.size() && nodoAux != nullptr) {
        nodoAux = nodoAux->siguientes[int(clave[i])];
        i++;
    }
    if (nodoAux != nullptr && nodoAux->definicion != nullptr) {
        res = 1;
    }
    return res;
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo *nodoAux = raiz;
    for (char c : clave) {
        nodoAux = nodoAux->siguientes[int(c)];
    }
    return *(nodoAux->definicion);
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo *nodoAux = raiz;
    for (char c : clave) {
        nodoAux = nodoAux->siguientes[int(c)];
    }
    return *(nodoAux->definicion);
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    Nodo *nodoAux = raiz;
    Nodo *aPartir = raiz;
    bool esPrefijo = false;
    int iteracion = 0;
    int ultimoIndice = 0;

    for (char c : clave) { // Encuentro la
        int j = 0;
        for (int i = 0; i < 256; ++i) {
            if (nodoAux->siguientes[i] != nullptr)
                j++;
        }
        if (nodoAux->definicion != nullptr || j > 1) {
            aPartir = nodoAux;
            ultimoIndice = iteracion;
        }
        nodoAux = nodoAux->siguientes[int(c)];
        iteracion++;
    }

    for (int i = 0; i < 256; ++i) {
        if (nodoAux->siguientes[i] != nullptr) {
            esPrefijo = true;
        }
    }
    delete nodoAux->definicion;
    nodoAux->definicion = nullptr;

    if (!esPrefijo) { // Si no es prefijo, elimino desde el maximo prefijo (si tiene) en adelante
        nodoAux = aPartir->siguientes[int(clave[ultimoIndice])];
        aPartir->siguientes[int(clave[ultimoIndice])] = nullptr;
        ultimoIndice++;
        aPartir = nodoAux;
        while (ultimoIndice < clave.size()){
            nodoAux = aPartir->siguientes[int(clave[ultimoIndice])];
            ultimoIndice++;
            delete aPartir;
            aPartir = nodoAux;
        }
        delete aPartir;
    }
    _size--;
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    return _size == 0;
}

template<typename T>
void string_map<T>::destruir(string_map::Nodo *n) {
    if (n != nullptr) {
        for (Nodo *nodo : (*n).siguientes) {
            destruir(nodo);
        }
        delete n->definicion;
        delete n;
    }
}

template<typename T>
void string_map<T>::construir(string_map::Nodo *n, string_map::Nodo *m) {
    if (m->definicion != nullptr) {
        n->definicion = new T(*(m->definicion));
    }
    for (int i = 0; i < 256; ++i) {
        if (m->siguientes[i] != nullptr) {
            n->siguientes[i] = new Nodo;
            construir(n->siguientes[i], m->siguientes[i]);
        }
    }
}

template<typename T>
T &string_map<T>::operator[](const string &key) {
    if (count(key)){
        return at(key);
    } else {
        insert(make_pair(key, T()));
        return at(key);
    }
}







