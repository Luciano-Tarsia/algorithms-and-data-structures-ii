#ifndef MULTICONJUNTO
#define MULTICONJUNTO

#include <vector>

using namespace std;

template <class T>
class Multiconjunto {
public:
    Multiconjunto();
    void agregar(T x);
    int ocurrencias(T x) const;
    bool operator<=(Multiconjunto<T> otro) const;

private:
    vector<T> _representacion;
};

template <class T>
Multiconjunto<T>::Multiconjunto(){
}

template <class T>
void Multiconjunto<T>::agregar(T x){
    this-> _representacion.push_back(x);
}

template <class T>
int Multiconjunto<T>::ocurrencias(T x) const {
    int res = 0;
    for (int i = 0; i < _representacion.size() ; ++i) {
        if (_representacion[i] == x){
            res++;
        }
    }
    return res;
}

template <class T>
bool Multiconjunto<T>::operator<=(Multiconjunto<T> otro) const{
    bool res = true;
    for (int i = 0; i < _representacion.size() ; ++i) {
        if (this->ocurrencias(_representacion[i]) > otro.ocurrencias(_representacion[i])){
            res = false;
        }
    }
    return res;
}

#endif MULTICONJUNTO