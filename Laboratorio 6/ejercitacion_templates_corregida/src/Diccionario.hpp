#ifndef DICCIONARIO
#define DICCIONARIO

using namespace std;

template <class Clave, class Valor>
class Diccionario {
public:
    Diccionario();
    void definir(Clave k, Valor v);
    bool def(Clave k) const;
    Valor obtener(Clave k) const;
    vector<Clave> claves() const;

private:
    struct Asociacion {
        Asociacion(Clave k, Valor v);
        Clave clave;
        Valor valor;
    };
    vector<Asociacion> _asociaciones;
};

template <class Clave, class Valor>
Diccionario<Clave,Valor>::Diccionario() {
}

template <class Clave, class Valor>
Diccionario<Clave,Valor>::Asociacion::Asociacion(Clave k, Valor v) : clave(k), valor(v) {
}

template <class Clave, class Valor>
void Diccionario<Clave,Valor>::definir(Clave k, Valor v) {
    for (unsigned int i = 0; i < _asociaciones.size(); i++) {
        if (_asociaciones[i].clave == k) {
            _asociaciones[i].valor = v;
            return;
        }
    }
    _asociaciones.push_back(Asociacion(k, v));
}

template <class Clave, class Valor>
bool Diccionario<Clave,Valor>::def(Clave k) const {
    for (unsigned int i = 0; i < _asociaciones.size(); i++) {
        if (_asociaciones[i].clave == k) {
            return true;
        }
    }
    return false;
}

template <class Clave, class Valor>
Valor Diccionario<Clave,Valor>::obtener(Clave k) const {
    for (unsigned int i = 0; i < _asociaciones.size(); i++) {
        if (_asociaciones[i].clave == k) {
            return _asociaciones[i].valor;
        }
    }
    assert(false);
}

template <class Clave, class Valor>
vector<Clave> Diccionario<Clave,Valor>::claves() const{
    vector<Clave> res;
    for (int i = 0; i < _asociaciones.size() ; ++i) {
        res.push_back(_asociaciones[i].clave);
    }
    for (int k = 0; k < res.size() ; ++k) {
        Clave temp = res[k];
        int posicion = k;
        for (int j = k; j < res.size() ; ++j) {
            if (temp > res[j]){
                temp = res[j];
                posicion = j;
            }
            if (j == res.size() - 1){
                Clave temp2 = res[k];
                res[k] = temp;
                res[posicion] = temp2;
            }
        }
    }
    return res;
}

/*
#include "Diccionario.cpp"
*/
 #endif