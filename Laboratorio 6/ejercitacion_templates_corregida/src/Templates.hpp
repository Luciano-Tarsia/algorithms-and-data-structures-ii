#ifndef TEMPLATE
#define TEMPLATE

template<class T>
T cuadrado(T x) {
    return x*x;
}

template<class Contenedor, class Elem>
bool contiene(Contenedor s, Elem c) {
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == c) {
            return true;
        }
    }
    return false;
}


template<class Contenedor>
bool esPrefijo(Contenedor a, Contenedor b) {
    int i = 0;
    if(a.size() < b.size()) {
        while ( (a[i] == b[i]) && (i < a.size()) ){
            i++;
        }
    }
    return i == a.size();
}

template<class Contenedor, class Elem>
Elem maximo(Contenedor c) {
    Elem res = c[0];
    for (int i = 0; i < c.size() ; ++i) {
        if (res < c[i]){
            res = c[i];
        }
    }
    return res;
}

#endif