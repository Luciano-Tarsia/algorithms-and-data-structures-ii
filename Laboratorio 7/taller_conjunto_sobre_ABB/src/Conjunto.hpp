
#include "Conjunto.h"

template<class T>
Conjunto<T>::Conjunto(): _raiz(nullptr), _cardinal(0) {}

template<class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}

template<class T>
bool Conjunto<T>::pertenece(const T &clave) const {
    bool res = false;
    Nodo *aux = this->_raiz;
    while (aux != nullptr) {
        if ((*aux).valor == clave) {
            res = true;
            aux = nullptr;
        } else if ((*aux).valor < clave) {
            aux = (*aux).der;
        } else {
            aux = (*aux).izq;
        }
    }
    return res;
}

template<class T>
void Conjunto<T>::insertar(const T &clave) {
    if (!pertenece(clave)) { // Pre: el input no pertenece al conjunto
        Nodo *elemento = new Nodo(clave);
        Nodo *aux = this->_raiz;
        if (_raiz == nullptr) {
            _raiz = elemento;
        } else {
            while (aux != nullptr) {
                if ((*aux).valor < clave) {
                    if ((*aux).der == nullptr) {
                        (*aux).der = elemento;
                        aux = nullptr;
                    } else {
                        aux = (*aux).der;
                    }
                } else {
                    if ((*aux).izq == nullptr) {
                        (*aux).izq = elemento;
                        aux = nullptr;
                    } else {
                        aux = (*aux).izq;
                    }
                }
            }
        }
        this->_cardinal = this->_cardinal + 1;
    }
}

template<class T>
void Conjunto<T>::remover(const T &elemento) {
    if (pertenece(elemento)) {
        Nodo *padre = nodoPadre(elemento); // Es el padre del elemento a eliminar
        Nodo *aEliminar = nullptr; // Es el elemento a eliminar
        bool esHijoIzquierdo = false;

        if (padre != nullptr && (*padre).izq != nullptr && (*padre).izq->valor == elemento) {
            esHijoIzquierdo = true;
        }

        if ((*_raiz).valor == elemento) { // Me paro en el nodo del elemento a eliminar
            aEliminar = _raiz;
        } else {
            if ((*padre).valor < elemento) {
                aEliminar = (*padre).der;
            } else {
                aEliminar = (*padre).izq;
            }
        }

        if ((*aEliminar).der == nullptr && (*aEliminar).izq == nullptr) { // Caso 1: El nodo es una hoja
            if (aEliminar == _raiz) { // Caso que elimino la raiz
                _raiz = nullptr;
            } else {
                if (esHijoIzquierdo) {
                    (*padre).izq = nullptr;
                } else {
                    (*padre).der = nullptr;
                }
            }
            delete (aEliminar);

        } else if ((*aEliminar).der != nullptr &&
                   (*aEliminar).izq == nullptr) { // Caso 2 (a): el nodo tiene un único hijo derecho
            Nodo *hijoDer = (*aEliminar).der;
            if (aEliminar == _raiz) { // Caso que elimino la raiz
                _raiz = hijoDer;
            } else {
                if (esHijoIzquierdo) {
                    (*padre).izq = hijoDer;
                } else {
                    (*padre).der = hijoDer;
                }
            }
            delete (aEliminar);

        } else if ((*aEliminar).der == nullptr &&
                   (*aEliminar).izq != nullptr) { // Caso 2 (b): el nodo tiene un único hijo izquierdo
            Nodo *hijoIzq = (*aEliminar).izq;
            if (aEliminar == _raiz) { // Caso que elimino la raiz
                _raiz = hijoIzq;
            } else {
                if (esHijoIzquierdo) {
                    (*padre).izq = hijoIzq;
                } else {
                    (*padre).der = hijoIzq;
                }
            }
            delete (aEliminar);

        } else { // Caso 3: El elemento tiene 2 hijos
            T sucesorInmediato = siguiente(elemento);
            Nodo *nodoSucesorInmediato = _raiz;
            while (nodoSucesorInmediato->valor != sucesorInmediato && nodoSucesorInmediato != nullptr) {
                if (nodoSucesorInmediato->valor < sucesorInmediato) {
                    nodoSucesorInmediato = nodoSucesorInmediato->der;
                } else {
                    nodoSucesorInmediato = nodoSucesorInmediato->izq;
                }
            }

            if ((*aEliminar).der->valor == sucesorInmediato) { // El sucesor inmediato es el hijo derecho
                if (_raiz->valor == elemento) {
                    _raiz->der->izq = _raiz->izq;
                    _raiz = _raiz->der;
                } else {
                    if (esHijoIzquierdo) {
                        (*padre).izq = (*aEliminar).der;
                        (*aEliminar).der->izq = aEliminar->izq;
                    } else {
                        (*padre).der = (*aEliminar).der;
                        (*aEliminar).der->izq = aEliminar->izq;
                    }
                }
                delete (aEliminar);
            } else { // El sucesor inmediato se encuentra dentro del subárbol derecho de aEliminar
                Nodo *padreDelSucesorInmediato = nodoPadre(sucesorInmediato);
                if ((*padreDelSucesorInmediato).der != nullptr &&
                    (*padreDelSucesorInmediato).der->valor == sucesorInmediato) {
                    padreDelSucesorInmediato->der = padreDelSucesorInmediato->der->der;
                } else if ((*padreDelSucesorInmediato).izq->valor == sucesorInmediato) {
                    padreDelSucesorInmediato->izq = padreDelSucesorInmediato->izq->der;
                }
                aEliminar->valor = sucesorInmediato;
                delete (nodoSucesorInmediato);
            }
        }
        _cardinal = _cardinal - 1;
    }
}

template<class T>
const T &Conjunto<T>::siguiente(const T &clave) { // Pre: Se asume que hay un nodo siguiente
    Nodo *padre = nodoPadre(clave);
    Nodo *nodoClave = _raiz;
    bool esHijoIzquierdo = false;

    while ((*nodoClave).valor != clave) {
        if ((*nodoClave).valor < clave) {
            nodoClave = (*nodoClave).der;
        } else {
            nodoClave = (*nodoClave).izq;
        }
    }

    if (padre != nullptr && (*padre).izq != nullptr && (*padre).izq->valor == clave) {
        esHijoIzquierdo = true;
    }

    if ((*nodoClave).der != nullptr) { // Caso 1: Tiene un subarbol derecho
        nodoClave = (*nodoClave).der;
        while ((*nodoClave).izq != nullptr) {
            nodoClave = (*nodoClave).izq;
        }
        return (*nodoClave).valor;
    } else if (esHijoIzquierdo) { // Caso 2 (a): Es hijo izquierdo
        return (*padre).valor;
    } else { // Caso 2 (b): Es hijo derecho sin subarbol derecho
        Nodo *y = padre;
        while (y != nullptr && nodoClave == (*y).der) {
            nodoClave = y;
            y = nodoPadre((*nodoClave).valor);
        }
        return (*y).valor;
    }
}

template<class T>
const T &Conjunto<T>::minimo() const {
    Nodo *aux = this->_raiz;
    while ((*aux).izq != nullptr) {
        aux = (*aux).izq;
    }
    return (*aux).valor;
}

template<class T>
const T &Conjunto<T>::maximo() const {
    Nodo *aux = this->_raiz;
    while ((*aux).der != nullptr) {
        aux = (*aux).der;
    }
    return (*aux).valor;
}

template<class T>
unsigned int Conjunto<T>::cardinal() const {
    return this->_cardinal;
}

template<class T>
void Conjunto<T>::destruir(Conjunto::Nodo *n) {
    if (n != nullptr) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template<class T>
class Conjunto<T>::Nodo *Conjunto<T>::nodoPadre(const T &elem) {
    Nodo *aux = _raiz;
    if (!pertenece(elem) || ((*_raiz).valor == elem)) {
        aux = nullptr;
    } else {
        while (((*aux).der == nullptr || (*aux).der->valor != elem) &&
               ((*aux).izq == nullptr || (*aux).izq->valor != elem)) {
            if ((*aux).valor < elem) {
                aux = (*aux).der;
            } else {
                aux = (*aux).izq;
            }
        }
    }
    return aux;
}