# Logic behind the exercise called "remover"

The procedure for deleting a given node Z from a binary
search tree T:

 1. If Z has no left child, then we replace Z by its right child,
which may or may not be NIL. When Z’s right child is NIL, this case deals with
the situation in which Z has no children. When Z’s right child is non-NIL, this
case handles the situation in which Z has just one child, which is its right 
child.
 2. If Z has just one child, which is its left child, then we replace Z by its
left child.
 3. Otherwise, Z has both a left and a right child. We find Z’s successor Y,
which lies in Z’s right subtree and has no left child. We want to
splice Y out of its current location and have it replace Z in the tree.
  - If Y is Z’s right child, then we replace Z by Y, leaving Y’s right
  child alone.
  - Otherwise, Y lies within Z’s right subtree but is not Z’s right child. In this case,
  we first replace Y by its own right child, and then we replace Z by Y.
 
Source: Chapter 12, Introduction to Algorithms