#include "Lista.h"

Lista::Lista(): _length(0), _head(NULL), _last(NULL) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (_head != nullptr) {
        this -> eliminar(0);
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    if(this == &aCopiar){ // Para el caso que l = l
        return *this;
    }
    while (_head != nullptr) {
        this -> eliminar(0);
    }
    for (Nat i = 0; i < aCopiar.longitud(); i++){
        this -> agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo = new Nodo();
    nodo -> _data = elem;
    nodo->_anterior = nullptr;
    if (_length == 0){
        _head = nodo;
        _last = nodo;
        nodo -> _siguiente = nullptr;
    } else {
        _head->_anterior = nodo;
        nodo->_siguiente = this->_head;
        this->_head = nodo;
    }
    this -> _length = _length + 1;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo = new Nodo();
    nodo -> _data = elem;
    nodo -> _siguiente = nullptr;
    if (_length == 0) {
        _head = nodo;
        _last = nodo;
        nodo -> _anterior = nullptr;
    }
    else{
        nodo->_anterior = this->_last;
        _last->_siguiente = nodo;
        this->_last = nodo;
    }
    this -> _length = _length + 1;
}

void Lista::eliminar(Nat i) {
    if (_length == 1){ // Caso: Lista de un solo elemento
        Nodo* aux = _head;
        _head = nullptr;
        _last = nullptr;
        delete (aux);
    } else if (i == 0){ // Caso: elimino el primero de una lista con longitud mayor a 1
       Nodo* aux = _head ->_siguiente;
       delete(_head);
       _head = aux;
       aux -> _anterior = nullptr;
   } else if (i == _length - 1){ // Caso: elimino el último de una lista con longitud mayor a 1
       Nodo* aux = _last ->_anterior;
       delete(_last);
       _last = aux;
        aux -> _siguiente = nullptr;
   } else { // Caso: elimino un elemento del medio (no el primero, no el último, otro) de una lista de longitud mayor a 1
       Nodo* aEliminar = _head;
       for (int j = 0; j < i; ++j) { // Me paro en el elemento que quiero eliminar
           aEliminar = aEliminar->_siguiente;
       }
       Nodo* aEliminarSiguiente = aEliminar->_siguiente;
       Nodo* aEliminarAnterior = aEliminar -> _anterior;

       delete(aEliminar);
       aEliminarAnterior ->_siguiente = aEliminarSiguiente;
       aEliminarSiguiente -> _anterior = aEliminarAnterior;
   }
    _length = _length - 1;
}

int Lista::longitud() const {
    return _length;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* temp = _head;
    for (int j = 0; j < i ; ++j) {
        temp = temp -> _siguiente;
    }
    int& res = temp -> _data;
    return res;
}

int& Lista::iesimo(Nat i) {
    Nodo* temp = _head;
    for (int j = 0; j < i ; ++j) {
        temp = temp -> _siguiente;
    }
    int& res = temp -> _data;
    return res;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
